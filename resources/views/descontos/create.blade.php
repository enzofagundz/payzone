@extends('layouts.cruds.forms', [
    'title' => 'Novo Desconto',
])

@section('errors')
    <span>
        {{ $errors->first() }}
    </span>
@endsection

@section('form')
    <form
        action="{{ route('descontos.store', $funcionario) }}"
        method="POST"
        class="grid max-w-screen-md gap-4 mx-auto sm:grid-cols-2"
        id="desconto_create"
    >
        @csrf

        <input type="number" value="{{ $funcionario->id }}" name="funcionario_id" hidden>

        <div x-data="{ tipoDesconto: '' }">
            <label for="nome" class="label">
                <span class="label-text">Tipo de Desconto</span>
            </label>
            <select name="nome" x-on:change="tipoDesconto = $event.target.value" class="w-full input input-bordered">
                <option value="">Selecione...</option>
                <option value="Plano">Plano de Saúde</option>
                <option value="Faltas">Faltas Injustificadas</option>
                <option value="ValeTransporte">Vale Transporte</option>
                <option value="ValeAlimentacao">Vale Alimentação</option>
            </select>

            <div x-show="tipoDesconto === 'Vale Transporte' " x-transition>
                <label for="quantidadeValesUtilizados">
                    <span class="label-text">
                        Quantidade de Vales Utilizados
                    </span>
                </label>
                <input
                    type="number" class="w-full max-w-xs input input-bordered" name="quantidadeValesUtilizados" id="quantidadeValesUtilizados"
                >
            </div>

            <div x-show="tipoDesconto === 'Vale Transporte' " x-transition>
                <label for="valorPorVale">
                    <span class="label-text">
                        Valor Unitário do Vale
                    </span>
                </label>
                <input
                    type="decimal" class="w-full max-w-xs input input-bordered" name="valorPorVale" id="valorPorVale"
                >
            </div>

            <div x-show="tipoDesconto === 'Faltas' " x-transition>
                <label for="faltas">
                    <span class="label-text">
                        Quantidade de Faltas
                    </span>
                </label>
                <input
                    type="number" class="w-full max-w-xs input input-bordered" name="faltas" id="faltas"
                >
            </div>

            <div x-show="tipoDesconto === 'Vale Alimentação' " x-transition>
                <label for="valorTotal">
                    <span class="label-text">
                        Valor Total
                    </span>
                </label>
                <input
                    type="text" class="w-full max-w-xs input input-bordered" name="valor" id="valorTotal"
                >
            </div>

            <div>
                <label for="percentual" class="label">
                    <span class="label-text">Percentual</span>
                </label>
                <input
                    type="string" class="w-full max-w-xs input input-bordered" name="percentual" id="percentual"
                    x-bind:value="tipoDesconto === 'Faltas' ? 0 : ''"
                    x-bind:readonly="tipoDesconto === 'Faltas' ? true : false"
                >
            </div>
        </div>

        @component('components.inputs.date', [
            'field' => 'data_vigencia',
            'label' => 'Data de Vigência',
            'required' => true,
        ])
        @endcomponent

        <div class="flex justify-center mt-2 sm:col-span-2">
            <button class="w-full btn btn-primary">
                Cadastrar
            </button>
        </div>
    </form>
@endsection
