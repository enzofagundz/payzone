@extends('layouts.app')
@section('content')
    <div class="min-h-screen hero bg-base-200">
        <div class="text-center hero-content">
            <div class="max-w-md">
                <h1 class="text-5xl font-bold">
                    Bem vindo ao Payzone
                </h1>
                <p class="py-6">
                    O Payzone é um sistema de que permite a gestão de pagamentos de uma empresa.
                </p>
                <a class="btn btn-primary" href="{{ route('dashboard') }}">
                    Comece agora
                </a>
            </div>
        </div>
    </div>
@endsection