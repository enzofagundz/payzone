<div class="shadow-xl card bg-base-200">
    <div class="items-center text-center card-body">
        <h2 class="card-title">
            {{ $title }}
        </h2>
        <p>
            {{ $description }}
        </p>
        <div class="card-actions">
            <a href="{{ $link }}" class="btn btn-wide btn-outline btn-primary">Gerenciar</a>
        </div>
    </div>
</div>
