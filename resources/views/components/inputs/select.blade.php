<div class="w-full max-w-xs form-control">
    @isset($label)
        <label class="label" for="{{ $field }}">
            <span class="label-text">
                {{ $label }}
            </span>
        </label>
    @endisset
    
    <select
        name="{{ $field }}"
        id="{{ $field }}"
        class="w-full input input-bordered"
    >
        <option value="">
            Selecione...
        </option>
        @isset($options)
            @foreach ($options as $option)
                <option
                    value="{{ $option->id }}"
                    @if(isset($value) && $value == $option->id) selected @endif
                >
                    {{ $option->nome }}
                </option>
            @endforeach
        @endisset
    </select>
</div>