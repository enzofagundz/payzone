<a class="btn btn-circle {{ $class }}" href="{{ $route }}" @if (isset($target)) target="{{ $target }}"@endif>
    {{ $slot }}
</a>
