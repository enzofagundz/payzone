<div class="w-full max-w-xs form-control">
    <label class="label" for="{{ $field }}">
        @isset($label)
            <span class="label-text">
                {{ $label }}*
            </span>
        @endisset
        {{-- Segundo label para instruções --}}
        @isset($instructions)
            <span class="label-text-alt">
                {{ $instructions }}
            </span>
        @endisset
    </label>
    <input 
        name="{{ $field }}" 
        id="{{ $field }}" 
        type="{{ $type }}" 
        placeholder="Escreva aqui" 
        class="w-full max-w-xs input input-bordered"
        @if($required) required @endif
        value="{{ $value ?? '' }}"
        @isset($disabled) disabled @endisset
    />
</div>