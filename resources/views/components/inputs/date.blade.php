<div class="w-full max-w-xs form-control">
    <label class="label" for="{{ $field }}">
        @isset($label)
            <span class="label-text">
                {{ $label }}*
            </span>
        @endisset
        @isset($instructions)
            <span class="label-text-alt">
                {{ $instructions }}
            </span>
        @endisset
    </label>
    <input 
        name="{{ $field }}" 
        id="{{ $field }}" 
        type="date" 
        class="block px-5 py-2.5 w-full max-w-xs input input-bordered " 
        @if($required) required @endif
        value="{{ $value ?? '' }}"
    />
</div>