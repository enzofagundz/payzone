<div class="w-full py-6 sm:py-8 lg:py-12">
    <div class="px-4 md:px-8">
        <div class="px-4 py-6 bg-base-200 rounded-lg md:py-8 lg:py-12">
            <p class="mb-2 font-semibold md:mb-3 lg:text-lg text-accent">
                {{ $title }}
            </p>
            <h1 class="text-2xl font-bold md:text-3xl lg:text-4xl">
                {{ $description }}
            </h1>
        </div>
    </div>
</div>