@extends('layouts.app')
@section('content')
    @include('layouts.components.navbar')
    @component('layouts.components.header', [
        'description' => $description,
        'title' => $title,
    ])
    @endcomponent

    <div class="flex flex-row justify-end w-full px-4 mb-6 sm:px-8">
        @yield('buttons')
    </div>

    <div class="max-w-full px-4 mx-auto sm:px-8">
        @yield('table')
    </div>
@endsection
