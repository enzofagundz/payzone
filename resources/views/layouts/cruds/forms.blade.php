@extends('layouts.app')
@section('content')
    @include('layouts.components.navbar')
    <div class="py-6 sm:py-8 lg:py-12">
        @if($errors->any())
            <div class="mb-8 alert alert-error">
                <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6 stroke-current shrink-0" fill="none" viewBox="0 0 24 24">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                    d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z" />
                </svg>
                @yield('errors')
            </div>
        @endif
        
        <div class="px-4 mx-auto max-w-screen-2xl md:px-8">
            <div class="mb-10 md:mb-16">
                <h2 class="mb-4 text-2xl font-bold text-center md:mb-6 lg:text-3xl">
                    {{ $title }}
                </h2>
            </div>
            @yield('form')
        </div>
    </div>
    </section>
@endsection
