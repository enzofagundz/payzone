@extends('layouts.cruds.index', [
    'title' => 'Cargos',
    'description' => 'Lista de Cargos',
])

@section('buttons')
    @component('components.inputs.button', [
        'route' => route('cargos.create'),
        'class' => 'bg-primary hover:bg-primary-focus',
    ])
        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="#fff" class="w-6 h-6">
            <path stroke-linecap="round" stroke-linejoin="round" d="M12 4.5v15m7.5-7.5h-15" />
        </svg>
    @endcomponent

    @component('components.inputs.button', [
        'route' => route('dashboard'),
        'class' => 'bg-secondary hover:bg-secondary-focus'
    ])
        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="#fff" class="w-6 h-6">
            <path stroke-linecap="round" stroke-linejoin="round" d="M2.25 12l8.954-8.955c.44-.439 1.152-.439 1.591 0L21.75 12M4.5 9.75v10.125c0 .621.504 1.125 1.125 1.125H9.75v-4.875c0-.621.504-1.125 1.125-1.125h2.25c.621 0 1.125.504 1.125 1.125V21h4.125c.621 0 1.125-.504 1.125-1.125V9.75M8.25 21h8.25" />
        </svg>
    @endcomponent
@endsection

@section('table')
    @isset($cargos)
        <div class="overflow-x-auto">
            <table class="table table-lg">
                <thead>
                    <tr>
                        <th></th>
                        <th>
                            Nome
                        </th>
                        <th>
                            Descrição
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($cargos as $cargo)
                        <tr class="hover">
                            <th></th>
                            <td>
                                {{ $cargo->nome }}
                            </td>
                            <td>
                                {{ $cargo->descricao ?? 'Sem descrição'}}
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="mt-6">
                {{ $cargos->links() }}
            </div>
        </div>
    @endisset
@endsection
