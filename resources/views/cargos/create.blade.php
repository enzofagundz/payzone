@extends('layouts.cruds.forms', [
    'title' => 'Novo Cargo',
])

@section('errors')
    <span>
        {{ $errors->first() }}
    </span>
@endsection

@section('form')
    <form
        action="{{ route('cargos.store') }}"
        method="POST"
        class="grid max-w-screen-md gap-4 mx-auto sm:grid-cols-2"
        id="cargo_create"
    >

        @csrf

        @component('components.inputs.input', [
            'field' => 'nome',
            'label' => 'Nome',
            'required' => true,
            'type' => 'text',
        ])
            
        @endcomponent

        @component('components.inputs.input', [
            'field' => 'descricao',
            'label' => 'Descrição',
            'required' => true,
            'type' => 'text',
        ])
            
        @endcomponent
        
        <div class="flex justify-center mt-2 sm:col-span-2">
            <button class="w-full btn btn-primary">
                Cadastrar
            </button>
        </div>
    </form>
@endsection