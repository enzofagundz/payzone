@extends('layouts.cruds.index', [
    'title' => 'Relatórios',
    'description' => 'Lista de Relatórios',
])

@section('buttons')
    @component('components.inputs.button', [
        'route' => route('relatorios.create'),
        'class' => 'bg-primary hover:bg-primary-focus',
    ])
        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="#fff" class="w-6 h-6">
            <path stroke-linecap="round" stroke-linejoin="round" d="M12 4.5v15m7.5-7.5h-15" />
        </svg>
    @endcomponent

    @component('components.inputs.button', [
        'route' => route('dashboard'),
        'class' => 'bg-secondary hover:bg-secondary-focus',
    ])
        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="#fff"
            class="w-6 h-6">
            <path stroke-linecap="round" stroke-linejoin="round"
                d="M2.25 12l8.954-8.955c.44-.439 1.152-.439 1.591 0L21.75 12M4.5 9.75v10.125c0 .621.504 1.125 1.125 1.125H9.75v-4.875c0-.621.504-1.125 1.125-1.125h2.25c.621 0 1.125.504 1.125 1.125V21h4.125c.621 0 1.125-.504 1.125-1.125V9.75M8.25 21h8.25" />
        </svg>
    @endcomponent
@endsection

@section('table')
    @isset($relatorios)
        <div class="overflow-x-auto">
            @if ($tipo == 0)
                <table class="table table-lg">
                    <thead>
                        <tr>
                            <th>Nome Funcionário</th>
                            <th>Bruto</th>
                            <th>Acréscimo</th>
                            <th>Descontos</th>
                            <th>Líquidos</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($relatorios as $relatorio)
                            <tr class="hover">
                                <td>
                                    {{ $relatorio->funcionario->nome }}
                                </td>
                                <td>
                                    R$ {{ number_format($relatorio->folhaCalculada->salario_bruto, 2, ',', '.') }}
                                </td>
                                <td>
                                    R$ {{ number_format($relatorio->folhaCalculada->salario_adicionais, 2, ',', '.') }}
                                </td>
                                <td>
                                    R$ {{ number_format($relatorio->folhaCalculada->salario_descontos, 2, ',', '.') }}
                                </td>
                                <td>
                                    R$ {{ number_format($relatorio->folhaCalculada->salario_liquido, 2, ',', '.') }}
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>
                                Total
                            </th>
                            <th>
                                R$ {{ number_format($relatorios->sum('folhaCalculada.salario_bruto'), 2, ',', '.') }}
                            </th>
                            <th>
                                R$ {{ number_format($relatorios->sum('folhaCalculada.salario_adicionais'), 2, ',', '.') }}
                            </th>
                            <th>
                                R$ {{ number_format($relatorios->sum('folhaCalculada.salario_descontos'), 2, ',', '.') }}
                            </th>
                            <th>
                                R$ {{ number_format($relatorios->sum('folhaCalculada.salario_liquido'), 2, ',', '.') }}
                            </th>
                        </tr>
                    </tfoot>
                </table>
            @else
                <table class="table mt-8 table-lg">
                    <thead>
                        <tr>
                            <th>
                                Relatório Anual
                            </th>
                        </tr>
                        <tr>
                            <th>Mês</th>
                            <th>Bruto</th>
                            <th>Acréscimo</th>
                            <th>Descontos</th>
                            <th>Líquidos</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($relatorios as $relatorio)
                            <tr class="hover">
                                <td>
                                    {{ \Carbon\Carbon::createFromDate(null, $relatorio['mes'], null)->locale('pt_BR')->monthName }}
                                </td>
                                <td>
                                    R$ {{ number_format($relatorio['totalSalarioBruto'], 2, ',', '.') }}
                                </td>
                                <td>
                                    R$ {{ number_format($relatorio['totalAdicionais'], 2, ',', '.') }}
                                </td>
                                <td>
                                    R$ {{ number_format($relatorio['totalDescontos'], 2, ',', '.') }}
                                </td>
                                <td>
                                    R$ {{ number_format($relatorio['totalSalarioLiquido'], 2, ',', '.') }}
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>
                                Total
                            </th>
                            <th>
                                R$ {{ number_format(array_sum(Arr::pluck($relatorios, 'totalSalarioBruto')), 2, ',', '.') }}
                            </th>
                            <th>
                                R$ {{ number_format(array_sum(Arr::pluck($relatorios, 'totalAdicionais')), 2, ',', '.') }}
                            </th>
                            <th>
                                R$ {{ number_format(array_sum(Arr::pluck($relatorios, 'totalDescontos')), 2, ',', '.') }}
                            </th>
                            <th>
                                R$ {{ number_format(array_sum(Arr::pluck($relatorios, 'totalSalarioLiquido')), 2, ',', '.') }}
                            </th> 
                        </tr>
                    </tfoot>
                </table>
            @endif
        </div>
    @endisset
@endsection
