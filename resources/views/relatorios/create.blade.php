@extends('layouts.cruds.forms', [
    'title' => 'Novo Relatório',
])

@section('form')
    <form
        action="{{ route('relatorios.store') }}"
        method="POST" 
        class="grid max-w-screen-md gap-4 mx-auto sm:grid-cols-2"
        id="relatorio_create"
    >
        @csrf

        @component('components.inputs.select', [
            'field' => 'tipo',
            'label' => 'Tipo',
            'required' => true,
            'options' => array(
                (object) [
                    'id' => '0',
                    'nome' => 'Mensal'
                ],
                (object) [
                    'id' => '1',
                    'nome' => 'Anual'
                ],
            )
        ])

        @endcomponent

        @component('components.inputs.date', [
            'field' => 'data',
            'label' => 'Data',
            'required' => true,
        ])

        @endcomponent
        

        <div class="flex justify-center mt-2 sm:col-span-2">
            <button class="w-full btn btn-primary">
                Cadastrar
            </button>
        </div>
    </form>
@endsection
