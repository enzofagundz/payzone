@extends('layouts.cruds.forms', [
    'title' => 'Editar FGTS',
])

@section('errors')
    <span>
        {{ $errors->first() }}
    </span>
@endsection

@section('form')
    <form 
        action="{{ route('fgts.update', $fgts->id) }}" 
        class="grid max-w-screen-md gap-4 mx-auto sm:grid-cols-2"
        method="POST" id="fgts_edit"
    >
        @csrf
        @component('components.inputs.input', [
            'field' => 'percentual',
            'label' => 'Percentual',
            'required' => true,
            'type' => 'text',
            'instructions' => 'Sem o símbolo de porcentagem',
            'value' => $fgts->percentual,
        ])
        @endcomponent

        @component('components.inputs.date', [
            'field' => 'data_vigencia',
            'label' => 'Data de Vigência',
            'required' => true,
            'value' => $fgts->data_vigencia,
        ])
        @endcomponent

        <div class="flex justify-center mt-2 sm:col-span-2">
            <button class="w-full btn btn-primary">
                Editar
            </button>
        </div>
    </form>
@endsection
