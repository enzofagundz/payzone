{{-- @dd($dependente) --}}
@extends('layouts.app')
@section('content')
    @include('layouts.components.navbar')

    @component('layouts.components.header', [
        'description' => $dependente->nome,
        'title' => 'Dependente',
    ])
        
    @endcomponent

    <div class="flex flex-row justify-end w-full px-4 mb-6 sm:px-8">
        <button class="btn btn-error btn-circle" onclick="my_modal_1.showModal()">
            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="#fff" class="w-6 h-6">
                <path stroke-linecap="round" stroke-linejoin="round" d="M14.74 9l-.346 9m-4.788 0L9.26 9m9.968-3.21c.342.052.682.107 1.022.166m-1.022-.165L18.16 19.673a2.25 2.25 0 01-2.244 2.077H8.084a2.25 2.25 0 01-2.244-2.077L4.772 5.79m14.456 0a48.108 48.108 0 00-3.478-.397m-12 .562c.34-.059.68-.114 1.022-.165m0 0a48.11 48.11 0 013.478-.397m7.5 0v-.916c0-1.18-.91-2.164-2.09-2.201a51.964 51.964 0 00-3.32 0c-1.18.037-2.09 1.022-2.09 2.201v.916m7.5 0a48.667 48.667 0 00-7.5 0" />
            </svg>
        </button>

        <dialog id="my_modal_1" class="modal modal-error">
            <div class="modal-box">
                <h3 class="text-lg font-bold">
                    Tem certeza que deseja excluir o dependente?
                </h3>
                <form action="{{ route('dependentes.index', ['funcionario' => $funcionario, 'dependente' => $dependente]) }}" method="POST" id="dependente_destroy">
                    @csrf
                    @method('DELETE')
                    <button class="btn btn-red" type="submit">
                        Excluir
                    </button>
                    <a class="btn btn-gray" onclick="my_modal_1.close()">
                        Cancelar
                    </a>
                </form>
            </div>
        </dialog>
    </div>
@endsection