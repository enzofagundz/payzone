@extends('layouts.cruds.forms', [
    'title' => 'Novo Dependente',
])

@section('errors')
    <span>
        {{ $errors->first() }}
    </span>
@endsection

@section('errors')
    <span>
        {{ $errors->first() }}
    </span>
@endsection

@section('form')
    <form
        action="{{ route('dependentes.store', $funcionario) }}"
        method="POST"
        class="grid max-w-screen-md gap-4 mx-auto sm:grid-cols-2"
        id="dependente_create"    
    >

        @csrf

            @component('components.inputs.input', [
                'field' => 'nome',
                'label' => 'Nome',
                'required' => true,
                'type' => 'text',
            ])
            
            @endcomponent

            @component('components.inputs.input', [
                'field' => 'cpf',
                'label' => 'CPF',
                'required' => true,
                'type' => 'text',
                'instructions' => 'Somente números',
            ])
            
            @endcomponent
            
            @component('components.inputs.date', [
                'label' => 'Data de Nascimento',
                'required' => true,
                'field' => 'data_nascimento',
            ])
            
            @endcomponent

        @component('components.inputs.select', [
            'label' => 'Relação com o Funcionário',
            'field' => 'relacao',
            'required' => true,
            'options' => [
                (object) [
                    'id' => 'filho',
                    'nome' => 'Filho(a)',
                ],
                (object) [
                    'id' => 'conjuge',
                    'nome' => 'Cônjuge',
                ],
                (object) [
                    'id' => 'outros',
                    'nome' => 'Outros',
                ],
            ],
        ])
            
        @endcomponent
        


        
        <div class="flex justify-center mt-2 sm:col-span-2">
            <button class="w-full btn btn-primary">
                Cadastrar
            </button>
        </div>
    </form>
@endsection