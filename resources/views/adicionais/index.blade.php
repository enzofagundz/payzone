@extends('layouts.cruds.index', [
    'title' => $funcionario->nome,
    'description' => 'Lista de adicionais',
])

@section('buttons')
    @component('components.inputs.button', [
        'route' => route('adicionais.create', $funcionario),
        'class' => 'bg-primary hover:bg-primary-focus',
    ])
        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="#fff" class="w-6 h-6">
            <path stroke-linecap="round" stroke-linejoin="round" d="M12 4.5v15m7.5-7.5h-15" />
        </svg>
    @endcomponent

    @component('components.inputs.button', [
        'route' => route('funcionarios.show', $funcionario),
        'class' => 'bg-neutral hover:bg-neutral-focus',
    ])
        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="#fff" class="w-6 h-6">
            <path stroke-linecap="round" stroke-linejoin="round" d="M9 15L3 9m0 0l6-6M3 9h12a6 6 0 010 12h-3" />
        </svg>
    @endcomponent

    @component('components.inputs.button', [
        'route' => route('dashboard'),
        'class' => 'bg-secondary hover:bg-secondary-focus'
    ])
        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="#fff" class="w-6 h-6">
            <path stroke-linecap="round" stroke-linejoin="round" d="M2.25 12l8.954-8.955c.44-.439 1.152-.439 1.591 0L21.75 12M4.5 9.75v10.125c0 .621.504 1.125 1.125 1.125H9.75v-4.875c0-.621.504-1.125 1.125-1.125h2.25c.621 0 1.125.504 1.125 1.125V21h4.125c.621 0 1.125-.504 1.125-1.125V9.75M8.25 21h8.25" />
        </svg>
    @endcomponent
@endsection

@section('table')
    <div class="overflow-x-auto">
        <table class="table table-lg">
            <thead>
                <tr>
                    <th>
                        Nome
                    </th>
                    <th>
                        Valor
                    </th>
                    <th>
                        Data de vigência
                    </th>
                    <th>
                        Ações
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach ($adicionais as $adicional)
                    <tr class="hover">
                        <td>
                            {{ $adicional->nome }}
                        </td>
                        <td>
                            {{ $adicional->valor }}
                        </td>
                        <td>
                            {{ \Carbon\Carbon::parse($adicional->data_vigencia)->format('d/m/Y') }}
                        </td>
                        <td class="flex flex-col items-center justify-center">
                            <a href="{{ route('adicionais.show', ['funcionario' => $funcionario, 'adicional' => $adicional]) }}"
                                class="text-xs text-slate-700 hover:text-gray-600">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                    stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
                                    <path stroke-linecap="round" stroke-linejoin="round"
                                        d="M2.036 12.322a1.012 1.012 0 010-.639C3.423 7.51 7.36 4.5 12 4.5c4.638 0 8.573 3.007 9.963 7.178.07.207.07.431 0 .639C20.577 16.49 16.64 19.5 12 19.5c-4.638 0-8.573-3.007-9.963-7.178z" />
                                    <path stroke-linecap="round" stroke-linejoin="round"
                                        d="M15 12a3 3 0 11-6 0 3 3 0 016 0z" />
                                </svg>
                            </a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <div class="mt-6">
            {{ $adicionais->links() }}
        </div>
    </div>
@endsection
