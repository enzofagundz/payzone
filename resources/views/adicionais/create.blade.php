@extends('layouts.cruds.forms', [
    'title' => 'Novo Adicional',
])

@section('errors')
    <span>
        {{ $errors->first() }}
    </span>
@endsection

@section('form')
    <form
        action="{{ route('adicionais.store', $funcionario) }}"
        method="POST" 
        class="grid max-w-screen-md gap-4 mx-auto sm:grid-cols-2"
        id="form_adicional"
    >
        @csrf
        
        <div x-data="{ tipoAdicional: '' }">
            <label for="nome" class="label" >
                <span class="label-text">Tipo de Adicional</span>
            </label>
            <select 
                name="nome" 
                x-on:change="tipoAdicional = $event.target.value" 
                class="w-full input input-bordered"
            >
                <option value="">Selecione...</option>
                <option value="HorasExtras">Horas Extras</option>
                <option value="AdicionalNoturno">Adicional Noturno</option>
                <option value="AdicionalPericulosidade">Adicional de Periculosidade</option>
            </select>
        
            <div x-show="tipoAdicional === 'HorasExtras' " x-transition>
                <label for="horas_trabalhadas" class="label">
                    <span class="label-text">
                        Horas trabalhadas
                    </span>
                    <span class="label-text-alt">
                        Apenas as horas extras trabalhadas
                    </span>
                </label>
                <input type="text" name="horas_trabalhadas" class="w-full max-w-xs input input-bordered">
            </div>

            <div 
                x-show=" tipoAdicional === 'AdicionalPericulosidade' || tipoAdicional === 'AdicionalNoturno' " 
                x-transition
            >
                <label for="percentual" class="label">
                    Percentual
                </label>
                <input 
                    type="text" 
                    class="w-full max-w-xs input input-bordered"
                    name="percentual"
                    id="percentual"
                    x-bind:value="tipoAdicional === 'AdicionalPericulosidade' ? 30 : ''"
                    x-bind:readonly="tipoAdicional === 'AdicionalPericulosidade' ? true : false"
                >
            </div>
        </div>

        @component('components.inputs.date', [
            'field' => 'data_vigencia',
            'label' => 'Data de Vigência',
            'required' => true,
            'instructions' => 'Data em que o adicional passa a valer'
        ])
            
        @endcomponent

        <div class="flex justify-center mt-2 sm:col-span-2">
            <button class="w-full btn btn-primary">
                Cadastrar
            </button>
        </div>
    </form>
@endsection