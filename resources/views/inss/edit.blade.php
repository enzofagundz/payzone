@extends('layouts.cruds.forms', [
    'title' => 'Editar INSS',
])

@section('errors')
    <span>
        {{ $errors->first() }}
    </span>
@endsection

@section('form')
    <form
        action="{{ route('inss.update', $inss->id) }}"
        class="grid max-w-screen-md gap-4 mx-auto sm:grid-cols-2"
        method="POST"
        id="inss_edit"
    >
        @method('PATCH')
        @csrf
        @component('components.inputs.input', [
            'field' => 'percentual',
            'label' => 'Percentual',
            'required' => true,
            'type' => 'text',
            'instructions' => 'Sem o símbolo de porcentagem',
            'value' => $inss->percentual,
        ])
            
        @endcomponent
        
        @component('components.inputs.input', [
            'field' => 'valor_inicial',
            'label' => 'Valor Inicial',
            'required' => true,
            'type' => 'text',
            'value' => $inss->valor_inicial,
        ])
            
        @endcomponent
        
        @component('components.inputs.input', [
            'field' => 'valor_final',
            'label' => 'Valor Final',
            'required' => true,
            'type' => 'text',
            'value' => $inss->valor_final,
        ])
            
        @endcomponent

        @component('components.inputs.date', [
            'field' => 'data_vigencia',
            'label' => 'Data de Vigência',
            'required' => true,
            'value' => $inss->data_vigencia,
        ])
        
        @endcomponent

        <div class="flex justify-center mt-2 sm:col-span-2">
            <button class="w-full btn btn-primary">
                Editar
            </button>
        </div>
    </form>
@endsection