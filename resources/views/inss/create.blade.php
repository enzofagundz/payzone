@extends('layouts.cruds.forms', [
    'title' => 'Novo INSS',
])

@section('errors')
    <span>
        {{ $errors->first() }}
    </span>
@endsection

@section('form')
    <form action="{{ route('inss.store') }}" method="POST" class="grid max-w-screen-md gap-4 mx-auto sm:grid-cols-2"
        id="inss_create">

        @csrf

        @component('components.inputs.input', [
            'field' => 'percentual',
            'label' => 'Percentual',
            'required' => true,
            'type' => 'text',
            'instructions' => 'Sem o símbolo de porcentagem',
        ])
        
        @endcomponent

        @component('components.inputs.input', [
            'field' => 'valor_inicial',
            'label' => 'Valor Inicial',
            'required' => true,
            'type' => 'text',
        ])
        
        @endcomponent

        @component('components.inputs.input', [
            'field' => 'valor_final',
            'label' => 'Valor Final',
            'required' => true,
            'type' => 'text',
        ])
        
        @endcomponent

        @component('components.inputs.date', [
            'field' => 'data_vigencia',
            'label' => 'Data de Vigência',
            'required' => true,
        ])

        @endcomponent
        
        <div class="flex justify-center mt-2 sm:col-span-2">
            <button class="w-full btn btn-primary">
                Cadastrar
            </button>
        </div>
    </form>
@endsection
