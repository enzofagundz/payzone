@extends('layouts.cruds.forms', [
    'title' => 'Novo Funcionário',
])

@section('errors')
    <span>
        {{ $errors->first() }}
    </span>
@endsection

@section('form')
    <form action="{{ route('funcionarios.store') }}" 
        method="POST" 
        class="grid max-w-screen-md gap-4 mx-auto sm:grid-cols-2"
        id="funcionario_create">
        @csrf
        @component('components.inputs.input', [
            'field' => 'name',
            'label' => 'Nome',
            'required' => true,
            'type' => 'text',
        ])
        @endcomponent


        @component('components.inputs.input', [
            'field' => 'cpf',
            'label' => 'CPF',
            'required' => true,
            'type' => 'text',
            'instructions' => 'Somente números',
        ])
        @endcomponent


        @component('components.inputs.input', [
            'field' => 'carga_horaria',
            'label' => 'Caraga Horária',
            'required' => true,
            'type' => 'text',
            
        ])
        @endcomponent


        @component('components.inputs.date', [
            'label' => 'Data de Admissão',
            'required' => true,
            'field' => 'data_admissao',
        ])
        @endcomponent


        @component('components.inputs.input', [
            'field' => 'salario',
            'label' => 'Salário',
            'required' => true,
            'type' => 'text',
        ])
        @endcomponent

        @component('components.inputs.select', [
            'label' => 'Cargo',
            'field' => 'cargo_id',
            'required' => true,
            'options' => $cargos,
        ])
        @endcomponent

        <div class="flex justify-center mt-2 sm:col-span-2">
            <button class="w-full btn btn-primary">
                Cadastrar
            </button>
        </div>
    </form>
@endsection
