@extends('layouts.cruds.forms', [
    'title' => 'Editar Funcionário',
])

@section('form')
    <form action="{{ route('funcionarios.update', $funcionario) }}" class="grid max-w-screen-md gap-4 mx-auto sm:grid-cols-2"
        method="POST" id="funcionario_edit">
        @method('PATCH')
        @csrf
        @component('components.inputs.input', [
            'field' => 'name',
            'label' => 'Nome',
            'required' => true,
            'type' => 'text',
            'value' => $funcionario->nome,
        ])
        @endcomponent

        @component('components.inputs.input', [
            'field' => 'cpf',
            'label' => 'CPF',
            'required' => true,
            'type' => 'text',
            'instructions' => 'Somente números',
            'value' => $funcionario->cpf,
        ])
        @endcomponent

        @component('components.inputs.input', [
            'field' => 'carga_horaria',
            'label' => 'Caraga Horária',
            'required' => true,
            'type' => 'text',
            'value' => $funcionario->carga_horaria,
        ])
        @endcomponent

        @component('components.inputs.date', [
            'label' => 'Data de Admissão',
            'required' => true,
            'field' => 'data_admissao',
            'value' => $funcionario->data_admissao,
        ])
        @endcomponent

        @component('components.inputs.input', [
            'field' => 'salario',
            'label' => 'Salário',
            'required' => true,
            'type' => 'text',
            'value' => $funcionario->salario->last()->valor ?? '',
        ])
        @endcomponent

        @component('components.inputs.select', [
            'label' => 'Cargo',
            'field' => 'cargo_id',
            'required' => true,
            'options' => $cargos,
            'value' => $funcionario->cargo_id,
        ])
        @endcomponent

    
        
        <div class="flex justify-center mt-2 sm:col-span-2">
            <button class="w-full btn btn-primary">
                Editar
            </button>
        </div>
    </form>
@endsection
