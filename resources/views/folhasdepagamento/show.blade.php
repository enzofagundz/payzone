@extends('layouts.app')
@section('content')
    @component('layouts.components.navbar', [
        'title' => 'Folha de Pagamento',
        'description' => 'Dados da folha de pagamento',
    ])
    @endcomponent

    <section class="w-full max-w-5xl border">
        <div class="max-w-5xl mx-auto bg-white">
            <article class="overflow-hidden">
                <div class="bg-white rounded-b-md">
                    <div class="flex items-center justify-between m-4 border p-9 border-slate-200">
                        <div class="text-slate-700">
                            <p class="text-xl font-extrabold tracking-tight uppercase font-body">
                                Payzone
                            </p>
                            <p>
                                CNPJ: 00.000.000/0000-00
                            </p>
                        </div>
                        <div class="flex flex-col items-center text-slate-700">
                            <h2 class="text-xl font-extrabold tracking-tight uppercase font-body">
                                Recibo de Pagamento de Salário
                            </h2>
                            <p>
                                {{ \Carbon\Carbon::parse($folha->data)->format('d/m/Y') }}
                            </p>
                        </div>
                    </div>
                    <div class="flex items-center justify-between py-2 mx-4 my-2 border px-9 border-slate-200">
                        <div class="text-slate-700">
                            <p class="font-bold tracking-tight font-body">
                                Código:
                            </p>
                            <p>
                                {{ $funcionario->id }}
                            </p>
                        </div>
                        <div class="text-slate-700">
                            <p class="font-bold tracking-tight font-body">
                                Nome do Funcionário:
                            </p>
                            <p>
                                {{ strtoupper($funcionario->nome) }}
                            </p>
                        </div>
                        <div class="text-slate-700">
                            <p class="font-bold tracking-tight font-body">
                                Cargo:
                            </p>
                            <p>
                                {{ strtoupper($funcionario->cargo->nome) }}
                            </p>
                        </div>
                    </div>
                
                    <div class="m-4 border p-9">
                        <div class="overflow-x-auto">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th class="text-left">Descrição</th>
                                        <th class="text-left">Referência</th>
                                        <th class="text-left">Vencimentos</th>
                                        <th class="text-left">Descontos</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="text-left">SALÁRIOS</td>
                                        <td class="text-left">30d</td>
                                        <td class="text-left">R$ {{ number_format($folha->folhaCalculada->salario_bruto, 2, ',', '.') }}</td>
                                        <td class="text-left"></td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">INSS</td>
                                        <td class="text-left">{{ number_format($folha->folhaCalculada->percentual_inss, 2, ',', '.') }}%</td>
                                        <td class="text-left"></td>
                                        <td class="text-left">{{ number_format($folha->folhaCalculada->inss_calculado, 2, ',', '.') }}</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">IRRF</td>
                                        <td class="text-left">{{ number_format($folha->folhaCalculada->percentual_irrf, 2, ',', '.') }}%</td>
                                        <td class="text-left"></td>
                                        <td class="text-left">{{ number_format($folha->folhaCalculada->irrf_calculado, 2, ',', '.') }}</td>
                                    </tr>
                                    @foreach ($adicionais as $adicional)
                                        <tr>
                                            <td class="text-left">{{ strtoupper($adicional->adicional->nome) }}</td>
                                            <td class="text-left"></td>
                                            <td class="text-left">{{ number_format($adicional->adicional->valor, 2, ',', '.') }}</td>
                                            <td class="text-left"></td>
                                        </tr>
                                    @endforeach
                                    @foreach ($descontos as $desconto)
                                        <tr>
                                            <td class="text-left">{{ strtoupper($desconto->desconto->nome) }}</td>
                                            <td class="text-left"></td>
                                            <td class="text-left"></td>
                                            <td class="text-left">{{ number_format($desconto->desconto->valor, 2, ',', '.') }}</td>
                                        </tr>
                                    @endforeach
                                    <tr>
                                        <td class="text-left"></td>
                                        <td class="text-left"></td>
                                        <td class="font-bold text-left">Líquido a Receber</td>
                                        <td class="text-left">R$ {{ number_format($folha->folhaCalculada->salario_liquido, 2, ',', '.') }}</td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="w-5/6 mt-4">
                                <div class="flex flex-row items-start justify-between text-xs">
                                    <div class="text-center"><strong>Salário base</strong> <br>R$ {{ number_format($folha->folhaCalculada->salario_bruto, 2, ',', '.') }}</div>
                                    <div class="text-center"><strong>Base Cálc. INSS</strong> <br> R$ {{ number_format($folha->folhaCalculada->salario_bruto, 2, ',', '.') }}</div>
                                    <div class="text-center"><strong>FGTS do Mês </strong><br> R$ {{ number_format($folha->folhaCalculada->fgts, 2, ',', '.') }}</div>
                                    <div class="text-center"><strong>Base Cálc. IRRF </strong><br>R$ {{ number_format($folha->folhaCalculada->base_irrf, 2, ',', '.') }}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mt-4 p-9">
                    <div class="border-t pt-9 border-slate-200">
                        <div class="flex flex-row justify-end w-full px-4 mb-6 sm:px-8">
                            @component('components.inputs.button', [
                                'route' => route('folhaDePagamento.index', $funcionario),
                                'class' => 'bg-neutral hover:bg-neutral-focus',
                            ])
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                                    stroke="#fff" class="w-6 h-6">
                                    <path stroke-linecap="round" stroke-linejoin="round"
                                        d="M9 15L3 9m0 0l6-6M3 9h12a6 6 0 010 12h-3" />
                                </svg>
                            @endcomponent
                            
                            @component('components.inputs.button', [
                                'route' => route('folhaDePagamento.print', ['funcionario' => $funcionario, 'folha' => $folha]),
                                'class' => 'bg-primary hover:bg-primary-focus',
                                'target' => '_blank',
                            ])
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="#fff" class="w-6 h-6">
                                    <path stroke-linecap="round" stroke-linejoin="round" d="M6.72 13.829c-.24.03-.48.062-.72.096m.72-.096a42.415 42.415 0 0110.56 0m-10.56 0L6.34 18m10.94-4.171c.24.03.48.062.72.096m-.72-.096L17.66 18m0 0l.229 2.523a1.125 1.125 0 01-1.12 1.227H7.231c-.662 0-1.18-.568-1.12-1.227L6.34 18m11.318 0h1.091A2.25 2.25 0 0021 15.75V9.456c0-1.081-.768-2.015-1.837-2.175a48.055 48.055 0 00-1.913-.247M6.34 18H5.25A2.25 2.25 0 013 15.75V9.456c0-1.081.768-2.015 1.837-2.175a48.041 48.041 0 011.913-.247m10.5 0a48.536 48.536 0 00-10.5 0m10.5 0V3.375c0-.621-.504-1.125-1.125-1.125h-8.25c-.621 0-1.125.504-1.125 1.125v3.659M18 10.5h.008v.008H18V10.5zm-3 0h.008v.008H15V10.5z" />
                                </svg>
                            @endcomponent

                            <form
                                action="{{ route('folhaDePagamento.destroy', ['funcionario' => $funcionario, 'folha' => $folha]) }}"
                                method="POST">
                                @csrf
                                @method('DELETE')
                                <button 
                                    type="submit"
                                    class="btn-error hover:btn-error-focus btn btn-circle"
                                >
                                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="#fff" class="w-6 h-6">
                                        <path stroke-linecap="round" stroke-linejoin="round" d="M14.74 9l-.346 9m-4.788 0L9.26 9m9.968-3.21c.342.052.682.107 1.022.166m-1.022-.165L18.16 19.673a2.25 2.25 0 01-2.244 2.077H8.084a2.25 2.25 0 01-2.244-2.077L4.772 5.79m14.456 0a48.108 48.108 0 00-3.478-.397m-12 .562c.34-.059.68-.114 1.022-.165m0 0a48.11 48.11 0 013.478-.397m7.5 0v-.916c0-1.18-.91-2.164-2.09-2.201a51.964 51.964 0 00-3.32 0c-1.18.037-2.09 1.022-2.09 2.201v.916m7.5 0a48.667 48.667 0 00-7.5 0" />
                                    </svg>
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </article>
        </div>
    </section>
    </div>
@endsection
