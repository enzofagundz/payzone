@extends('layouts.cruds.forms', [
    'title' => 'Nova Folha de Pagamento',
])

@section('errors')
    <span>
        {{ $errors->first() }}
    </span>
@endsection

@section('form')
    <form
        action="{{ route('folhaDePagamento.store', $funcionario) }}"
        method="POST"
        class="grid max-w-screen-md gap-4 mx-auto sm:grid-cols-2"
    >

        @csrf

        <input 
            type="number" 
            value="{{ $funcionario->id }}"
            name="funcionario_id"
            hidden
        >

        @component('components.inputs.select', [
            'field' => 'tipo',
            'label' => 'Tipo de Folha',
            'options' => array(
                (object) [
                    'id' => '1',
                    'nome' => 'Folha de Pagamento'
                ],
                (object) [
                    'id' => '2',
                    'nome' => 'Férias'
                ],
                (object) [
                    'id' => '3',
                    'nome' => '13º Salário'
                ],
                (object) [
                    'id' => '4',
                    'nome' => 'Rescisão'
                ],
            )
        ])
            
        @endcomponent

        @component('components.inputs.date', [
            'field' => 'data',
            'label' => 'Data',
            'required' => true,
        ])

        @endcomponent
        
        <div class="flex justify-center mt-2 sm:col-span-2">
            <button class="w-full btn btn-primary">
                Cadastrar
            </button>
        </div>
    </form>
@endsection