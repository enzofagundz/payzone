<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <style>
        body {
            font-family: 'Roboto', sans-serif;
        }
        
        table {
            width: 100%;
            border-collapse: collapse;
            border-spacing: 0;
            border: 1px solid #ddd;
        }

        th, td {
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #f2f2f2
        }

        .table {
            border-collapse: collapse;
            border-spacing: 0;
            width: 100%;
            border: 1px solid #ddd;
        }

        .rounded-b-md {
            border-bottom-left-radius: 0.25rem;
            border-bottom-right-radius: 0.25rem;
        }
    </style>
    <title>
        Recibo de Pagamento de Salário
    </title>
</head>

<body>
    <div class="rounded-b-md">
        <div class="flex items-center justify-between m-4 border p-9 border-slate-200">
            <div class="text-slate-700">
                <p class="text-xl font-extrabold tracking-tight uppercase font-body">
                    Payzone
                </p>
                <p>
                    CNPJ: 00.000.000/0000-00
                </p>
            </div>
            <div class="flex flex-col items-center text-slate-700">
                <h2 class="text-xl font-extrabold tracking-tight uppercase font-body">
                    Recibo de Pagamento de Salário
                </h2>
                <p>
                    {{ \Carbon\Carbon::parse($folha->data)->format('d/m/Y') }}
                </p>
            </div>
        </div>
        <div class="flex items-center justify-between py-2 mx-4 my-2 border px-9 border-slate-200">
            <div class="text-slate-700">
                <p class="font-bold tracking-tight font-body">
                    Código:
                </p>
                <p>
                    {{ $funcionario->id }}
                </p>
            </div>
            <div class="text-slate-700">
                <p class="font-bold tracking-tight font-body">
                    Nome do Funcionário:
                </p>
                <p>
                    {{ strtoupper($funcionario->nome) }}
                </p>
            </div>
            <div class="text-slate-700">
                <p class="font-bold tracking-tight font-body">
                    Cargo:
                </p>
                <p>
                    {{ strtoupper($funcionario->cargo->nome) }}
                </p>
            </div>
        </div>
        <div class="m-4 border p-9">
            <div class="overflow-x-auto">
                <table class="table">
                    <thead>
                        <tr>
                            <th class="text-left">Descrição</th>
                            <th class="text-left">Referência</th>
                            <th class="text-left">Vencimentos</th>
                            <th class="text-left">Descontos</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="text-left">SALÁRIOS</td>
                            <td class="text-left">30d</td>
                            <td class="text-left">R$
                                {{ number_format($folha->folhaCalculada->salario_bruto, 2, ',', '.') }}</td>
                            <td class="text-left"></td>
                        </tr>
                        <tr>
                            <td class="text-left">INSS</td>
                            <td class="text-left">
                                {{ number_format($folha->folhaCalculada->percentual_inss, 2, ',', '.') }}%</td>
                            <td class="text-left"></td>
                            <td class="text-left">
                                {{ number_format($folha->folhaCalculada->inss_calculado, 2, ',', '.') }}</td>
                        </tr>
                        <tr>
                            <td class="text-left">IRRF</td>
                            <td class="text-left">
                                {{ number_format($folha->folhaCalculada->percentual_irrf, 2, ',', '.') }}%</td>
                            <td class="text-left"></td>
                            <td class="text-left">
                                {{ number_format($folha->folhaCalculada->irrf_calculado, 2, ',', '.') }}</td>
                        </tr>
                        @foreach ($adicionais as $adicional)
                            <tr>
                                <td class="text-left">{{ strtoupper($adicional->adicional->nome) }}</td>
                                <td class="text-left"></td>
                                <td class="text-left">{{ number_format($adicional->adicional->valor, 2, ',', '.') }}
                                </td>
                                <td class="text-left"></td>
                            </tr>
                        @endforeach
                        @foreach ($descontos as $desconto)
                            <tr>
                                <td class="text-left">{{ strtoupper($desconto->desconto->nome) }}</td>
                                <td class="text-left"></td>
                                <td class="text-left"></td>
                                <td class="text-left">{{ number_format($desconto->desconto->valor, 2, ',', '.') }}</td>
                            </tr>
                        @endforeach
                        <tr>
                            <td class="text-left"></td>
                            <td class="text-left"></td>
                            <td class="font-bold text-left">Líquido a Receber</td>
                            <td class="text-left">R$
                                {{ number_format($folha->folhaCalculada->salario_liquido, 2, ',', '.') }}</td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td class="text-left">
                                <strong>Salário base</strong> <br>R$
                                {{ number_format($folha->folhaCalculada->salario_bruto, 2, ',', '.') }}
                            </td>
                            <td class="text-left">
                                <strong>Salário base</strong> <br>R$
                                {{ number_format($folha->folhaCalculada->salario_bruto, 2, ',', '.') }}
                            </td>
                            <td class="text-left">
                                <strong>FGTS do Mês </strong><br> R$
                                {{ number_format($folha->folhaCalculada->fgts, 2, ',', '.') }}
                            </td>
                            <td class="text-left">
                                <strong>Base Cálc. IRRF </strong><br>R$
                                {{ number_format($folha->folhaCalculada->base_irrf, 2, ',', '.') }}
                            </td>
                                
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</body>

</html>
