<?php

use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\FGTSController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\INSSController;
use App\Http\Controllers\IRRFController;
use App\Http\Controllers\CargoController;
use App\Http\Controllers\DescontoController;
use App\Http\Controllers\AdicionalController;
use App\Http\Controllers\RelatorioController;
use App\Http\Controllers\DependenteController;
use App\Http\Controllers\FuncionarioController;
use App\Http\Controllers\FolhaDePagamentoController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

require __DIR__.'/auth.php';

Route::resource('funcionarios', FuncionarioController::class);

Route::get('dependentes/{funcionario}', [DependenteController::class, 'index'])->name('dependentes.index');
Route::get('dependentes/{funcionario}/create', [DependenteController::class, 'create'])->name('dependentes.create');
Route::post('dependentes/{funcionario}', [DependenteController::class, 'store'])->name('dependentes.store');
Route::get('dependentes/{funcionario}/{dependente}', [DependenteController::class, 'show'])->name('dependente.show');
Route::get('dependentes/{funcionario}/{dependente}/edit', [DependenteController::class, 'edit'])->name('dependente.edit');
Route::delete('dependentes/{funcionario}/{dependente}', [DependenteController::class, 'destroy'])->name('dependente.destroy');

Route::get('adicional/{funcionario}', [AdicionalController::class, 'index'])->name('adicional.index');
Route::get('adicional/{funcionario}/create', [AdicionalController::class, 'create'])->name('adicionais.create');
Route::post('adicionais/{funcionario}', [AdicionalController::class, 'store'])->name('adicionais.store');
Route::get('adicional/{funcionario}/{adicional}', [AdicionalController::class, 'show'])->name('adicionais.show');
Route::delete('adicional/{funcionario}/{adicional}', [AdicionalController::class, 'destroy'])->name('adicionais.destroy');

Route::get('descontos/{funcionario}', [DescontoController::class, 'index'])->name('descontos.index');
Route::get('descontos/{funcionario}/create', [DescontoController::class, 'create'])->name('descontos.create');
Route::post('descontos/{funcionario}', [DescontoController::class, 'store'])->name('descontos.store');
Route::get('descontos/{funcionario}/{desconto}', [DescontoController::class, 'show'])->name('descontos.show');
Route::delete('descontos/{funcionario}/{desconto}', [DescontoController::class, 'destroy'])->name('descontos.destroy'); 

Route::get('folhaDePagamento/{funcionario}', [FolhaDePagamentoController::class, 'index'])->name('folhaDePagamento.index');
Route::get('folhaDePagamento/{funcionario}/create', [FolhaDePagamentoController::class, 'create'])->name('folhaDePagamento.create');
Route::post('folhaDePagamento/{funcionario}', [FolhaDePagamentoController::class, 'store'])->name('folhaDePagamento.store');
Route::get('folhaDePagamento/{funcionario}/{folhaId}', [FolhaDePagamentoController::class, 'show'])->name('folhaDePagamento.show');
Route::delete('folhaDePagamento/{funcionario}/{folha}', [FolhaDePagamentoController::class, 'destroy'])->name('folhaDePagamento.destroy');
Route::get('folhaDePagamento/{funcionario}/{folha}/print', [FolhaDePagamentoController::class, 'print'])->name('folhaDePagamento.print');

Route::resource('inss', INSSController::class);
Route::resource('irrf', IRRFController::class);
Route::resource('cargos', CargoController::class);
Route::resource('fgts', FGTSController::class);
Route::resource('relatorios', RelatorioController::class);