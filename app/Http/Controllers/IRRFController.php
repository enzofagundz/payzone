<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreIRRFRequest;
use App\Models\IRRF;
use App\Services\IRRFService;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class IRRFController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(): View
    {
        return view('irrf.index', [
            'irrf' => IRRF::orderBy('data_vigencia', 'desc')->paginate(10),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): View
    {
        return view('irrf.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreIRRFRequest $request): View | RedirectResponse
    {
        $data = $request->validated();
        $data['percentual'] = (float) str_replace(',', '.', $data['percentual']);
        $data['valor_inicial'] = (float) str_replace(',', '.', $data['valor_inicial']);
        $data['valor_final'] = (float) str_replace(',', '.', $data['valor_final']);

        if (IRRFService::verificarIRRFCadastrado($data)) {
            return redirect()->back()->withErrors(['Já existe um IRRF cadastrado com esses valores para a data de vigência.']);
        }
        
        IRRF::create($data);

        return redirect()->route('irrf.index');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id): View
    {
        $irrf = IRRF::findOrFail($id);
        return view('irrf.show', compact('irrf'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id): View
    {
        $irrf = IRRF::findOrFail($id);
        return view('irrf.edit', compact('irrf'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(StoreIRRFRequest $request, string $id): View | RedirectResponse
    {
        $data = $request->validated();

        $data['percentual'] = (float) str_replace(',', '.', $data['percentual']);
        $data['valor_inicial'] = (float) str_replace(',', '.', $data['valor_inicial']);
        $data['valor_final'] = (float) str_replace(',', '.', $data['valor_final']);

        if (IRRFService::verificarIRRFCadastrado($data)) {
            return redirect()->back()->withErrors(['Já existe um IRRF cadastrado com esses valores para a data de vigência.']);
        }

        $irrf = IRRF::findOrFail($id);
        $irrf->update($data);
        return redirect()->route('irrf.index');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id): View | RedirectResponse
    {
        $irrf = IRRF::findOrFail($id);
        $irrf->delete();
        return redirect()->route('irrf.index');
    }
}
