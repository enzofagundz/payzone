<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreDescontoRequest;
use App\Models\Desconto;
use App\Models\Funcionario;
use App\Services\DescontoService;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class DescontoController extends Controller
{
    public function index(Funcionario $funcionario): View
    {
        return view('descontos.index', [
            'descontos' => $funcionario->funcionarioHasDesconto()->paginate(10),
            'funcionario' => $funcionario,
        ]);
    }

    public function create(Funcionario $funcionario): View
    {
        return view('descontos.create', compact('funcionario'));
    }

    public function store(StoreDescontoRequest $request, Funcionario $funcionario): RedirectResponse
    {
        // faz a validação do request em StoreDescontoRequest
        $data = $request->validated();

        // converte a string para float
        $data['percentual'] = (float) str_replace(',', '.', $data['percentual']);
        $data['salario_base'] = (float) $funcionario->salario()->latest('created_at')->first()->valor;
        $data['valorPorVale'] = (float) str_replace(',', '.', $data['valorPorVale']);

        $data['funcionario'] = $funcionario;
        $data['funcionario_id'] = $funcionario->id;
        
        $desconto = DescontoService::criarDesconto($data['nome']);
        $data['valor'] = $desconto->calcularDesconto($data);

        Desconto::create($data);

        return redirect()->route('descontos.index', $funcionario);
    }

    public function show(Funcionario $funcionario, Desconto $desconto): View
    {
        return view('descontos.show', compact('desconto', 'funcionario'));
    }

    public function destroy(Funcionario $funcionario, Desconto $desconto): RedirectResponse
    {
        $desconto->delete();
        return redirect()->route('descontos.index', $funcionario);
    }
}
