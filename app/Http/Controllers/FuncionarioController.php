<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreFuncionarioRequest;
use App\Models\Cargo;
use App\Models\Salario;
use App\Models\Funcionario;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;

class FuncionarioController extends Controller
{
    public function index(Request $request): View
    {
        $pesquisa = $request->get('cpf');

        if (!$pesquisa) {
            return view('funcionarios.index', [
                'funcionarios' => Funcionario::paginate(10),
            ]);
        }
    
        return view('funcionarios.index', [
            'funcionarios' => Funcionario::where('cpf', 'like', "%{$pesquisa}%")->paginate(10),
        ]);
    }

    public function create(): View
    {
        $cargos = Cargo::all();
        return view('funcionarios.create', compact('cargos'));
    }

    public function store(StoreFuncionarioRequest $request): RedirectResponse
    {
        $data = $request->validated();
        $data['salario'] = (float) str_replace(',', '.', $data['salario']);
        
        $funcionario = Funcionario::create($data);

        $this->cadastrarSalario($funcionario->id, $data['salario'], $data['data_admissao']);
        return redirect()->route('funcionarios.index');
    }

    public function cadastrarSalario($funcionario_id, $salario, $data_vigencia)
    {
        Salario::create([
            'valor' => $salario,
            'funcionario_id' => $funcionario_id,
            'data_vigencia' => $data_vigencia,
        ]);
    }

    public function show(Funcionario $funcionario)
    {
        $pages = [
            [
                'title' => 'Dependentes',
                'description' => 'Lista de dependentes do funcionário',
                'link' => route('dependentes.index', $funcionario),
            ],
            [
                'title' => 'Adicionais',
                'description' => 'Lista de adicionais do funcionário',
                'link' => route('adicional.index', $funcionario),
            ],
            [
                'title' => 'Descontos',
                'description' => 'Lista de descontos do funcionário',
                'link' => route('descontos.index', $funcionario),
            ],
            [
                'title' => 'Folhas de Pagamento',
                'description' => 'Lista de folhas de pagamento do funcionário',
                'link' => route('folhaDePagamento.index', $funcionario),
            ]
        ];

        return view('funcionarios.show', compact('funcionario', 'pages'));
    }

    public function edit(Funcionario $funcionario)
    {
        $cargos = Cargo::all();
        return view('funcionarios.edit', compact('funcionario', 'cargos'));
    }

    public function update(StoreFuncionarioRequest $request, Funcionario $funcionario)
    {
        $data = $request->validated();

        $data['salario'] = (float) str_replace(',', '.', $data['salario']);
        $data['salario'] = (float) str_replace(',', '.', $data['salario']);

        $funcionario->update([
            'nome' => $data['name'],
            'data_admissao' => $data['data_admissao'],
            'cpf' => $data['cpf'],
            'carga_horaria' => $data['carga_horaria'],
            'cargo_id' => $data['cargo_id'],
        ]);

        $this->cadastrarSalario($funcionario->id, $data['salario'], $data['data_admissao']);
        return redirect()->route('funcionarios.index');
    }

    public function destroy(Funcionario $funcionario)
    {
        $funcionario->delete();
        return redirect()->route('funcionarios.index');
    }
}
