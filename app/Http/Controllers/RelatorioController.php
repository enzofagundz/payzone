<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreRelatorioRequest;
use App\Models\FolhaDePagamento;
use App\Services\RelatorioService;
use Illuminate\Contracts\View\View;

class RelatorioController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(): View
    {
        return view('relatorios.index');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): View
    {
        return view('relatorios.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreRelatorioRequest $request): View
    {
        $data = $request->validated();
        if ($data['tipo'] == 0) {
            $relatorios = FolhaDePagamento::where('data', $data['data'])
                ->with(['funcionario', 'tipoFolha', 'folhaPagamentoHasAdicional', 'folhaPagamentoHasDesconto', 'inss', 'irrf', 'folhaCalculada'])
                ->get();
        } else {
            $relatorios = FolhaDePagamento::whereYear('data', $data['data'])
                ->with(['funcionario', 'tipoFolha', 'folhaPagamentoHasAdicional', 'folhaPagamentoHasDesconto', 'inss', 'irrf', 'folhaCalculada'])
                ->get();
            $relatorios = RelatorioService::calcularRelatorioAnual($relatorios);
        }

        return view('relatorios.index', [
            'relatorios' => $relatorios,
            'tipo' => $data['tipo'],
        ]);
    }
}
