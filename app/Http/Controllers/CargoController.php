<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreCargoRequest;
use App\Models\Cargo;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class CargoController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(): View
    {
        return view('cargos.index', [
            'cargos' => Cargo::paginate(10),
        ]);
    }

    public function create(): View
    {
        return view('cargos.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreCargoRequest $request): RedirectResponse
    {
        // faz a validação do request em app/Http/Requests/StoreCargoRequest.php
        $request->validated();
        Cargo::create($request->all());
        return redirect()->route('cargos.index');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id): RedirectResponse
    {
        $cargo = Cargo::findOrFail($id);
        $cargo->delete();
        return redirect()->route('cargos.index');
    }
}
