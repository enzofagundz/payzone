<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreDependenteRequest;
use App\Models\Dependente;
use App\Models\Funcionario;
use App\Services\DependenteService;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class DependenteController extends Controller
{
    public function index(Funcionario $funcionario): View
    {
        return view('dependentes.index', [
            'dependentes' => $funcionario->funcionarioHasDependente()->paginate(10),
            'funcionario' => $funcionario,
        ]);
    }

    public function create(Funcionario $funcionario): View
    {
        return view('dependentes.create', compact('funcionario'));
    }

    public function store(StoreDependenteRequest $request, Funcionario $funcionario) 
    {        
        // faz a validação do request em app/Http/Requests/StoreDependenteRequest.php
        $data = $request->validated();
        $data['funcionario_id'] = $funcionario->id;
        if(DependenteService::verificarIdadeDependente($data)) {
            return redirect()->back()->withErrors(['data_nascimento' => 'Dependente não pode ser maior de 21 anos']);
        }

        $dependente = Dependente::create($data);
        $data['dependente_id'] = $dependente->id;
        $funcionario->funcionarioHasDependente()->create($data);

        return redirect()->route('dependentes.index', $funcionario);
    }

    public function show(Funcionario $funcionario, Dependente $dependente): View
    {
        return view('dependentes.show', compact('funcionario', 'dependente'));
    }

    public function edit(Funcionario $funcionario, Dependente $dependente): View
    {
        return view('dependentes.edit', compact('funcionario', 'dependente'));
    }

    public function update(StoreDependenteRequest $request, Funcionario $funcionario, Dependente $dependente): RedirectResponse
    {
        // faz a validação do request em app/Http/Requests/StoreDependenteRequest.php
        $request->validated();
        $data = $request->all();

        if(DependenteService::verificarIdadeDependente($data)) {
            return redirect()->back()->withErrors(['data_nascimento' => 'Dependente não pode ser maior de 21 anos']);
        }

        $dependente->update($data);
        $funcionario->funcionarioHasDependente()->update($data);
        return redirect()->route('dependentes.index', $funcionario);
    }

    public function destroy(Funcionario $funcionario, Dependente $dependente): RedirectResponse
    {
        $dependente->delete();
        return redirect()->route('dependentes.index', $funcionario);
    }
}