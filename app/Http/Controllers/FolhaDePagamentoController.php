<?php

namespace App\Http\Controllers;

use App\Models\FolhaCalculada;
use App\Models\INSS;
use App\Models\IRRF;
use App\Models\Funcionario;
use Illuminate\Http\Request;
use App\Models\FolhaDePagamento;
use App\Models\FolhaPagamentoHasDesconto;
use App\Services\FolhaDePagamentoService;
use App\Models\FolhaPagamentoHasAdicional;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class FolhaDePagamentoController extends Controller
{
    public function index(Funcionario $funcionario): View
    {
        return view('folhasdepagamento.index', [
            'folhas' => $funcionario->folhaDePagamento()->paginate(10),
            'funcionario' => $funcionario,
        ]);
    }

    public function create(Funcionario $funcionario): View
    {
        return view('folhasdepagamento.create', compact('funcionario'));
    }

    public function store(Funcionario $funcionario, Request $request): RedirectResponse
    {
        $data = $request->validate([
            'tipo' => 'required|in:1,4',
            'data' => 'required|date',
        ]);

        $folhaCalculada = FolhaDePagamentoService::calcularFolhaPagamento($funcionario, $data['data']);

        $irrf = IRRF::findOrFail($folhaCalculada['irrf']->id);
        $inss = INSS::findOrFail($folhaCalculada['inss']->id);

        $folhaCadastrada = FolhaDePagamento::create([
            'tipo_folha_id' => $data['tipo'],
            'data' => $data['data'],
            'funcionario_id' => $funcionario->id,
            'irrf_id' => $irrf->id,
            'inss_id' => $inss->id,
        ]);

        if(!$folhaCadastrada) {
            return redirect()->back()->withErrors('Erro ao cadastrar folha de pagamento');
        }

        $folhaAdicionais = [];
        foreach ($folhaCalculada['adicionais'] as $adicional) {
            $folhaAdicionais[] = [
                'folha_de_pagamento_id' => $folhaCadastrada->id,
                'adicional_id' => $adicional->id,
            ];
        }

        FolhaPagamentoHasAdicional::insert($folhaAdicionais);

        $folhaDescontos = [];
        foreach ($folhaCalculada['descontos'] as $desconto) {
            $folhaDescontos[] = [
                'folha_de_pagamento_id' => $folhaCadastrada->id,
                'desconto_id' => $desconto->id,
            ];
        }

        FolhaPagamentoHasDesconto::insert($folhaDescontos);

        FolhaCalculada::create([
            'salario_adicionais' => $folhaCalculada['salario_adicionais'],
            'salario_descontos' => $folhaCalculada['salario_descontos'],
            'fgts' => $folhaCalculada['fgts'],
            'inss_calculado' => $folhaCalculada['inss_calculado'],
            'irrf_calculado' => $folhaCalculada['irrf_calculado'],
            'salario_liquido' => $folhaCalculada['salario_liquido'],
            'folha_de_pagamento_id' => $folhaCadastrada->id,
            'percentual_inss' => $folhaCalculada['percentual_inss'],
            'percentual_irrf' => $folhaCalculada['percentual_irrf'],
            'base_irrf'=> $folhaCalculada['base_irrf'],
            'quantidade_dependentes' => $folhaCalculada['quantidade_dependentes'],
            'salario_bruto' => $folhaCalculada['salario_bruto'],
        ]);

        return redirect()->route('folhaDePagamento.index', $funcionario);
    }

    public function show(Funcionario $funcionario, String $folhaId): View
    {
        $folha = FolhaDePagamento::findOrFail($folhaId);
        $adicionais = $folha->folhaPagamentoHasAdicional()->get();
        $descontos = $folha->folhaPagamentoHasDesconto()->get();
        return view('folhasdepagamento.show', compact('funcionario', 'folha', 'adicionais', 'descontos'));
    }

    public function destroy(Funcionario $funcionario, $folha): RedirectResponse
    {
        $folha->delete();
        return redirect()->route('folhaDePagamento.index', $funcionario);
    }

    public function print(Funcionario $funcionario, String $folha)
    {
        $folha = FolhaDePagamento::findOrFail($folha);
        $adicionais = $folha->folhaPagamentoHasAdicional()->get();
        $descontos = $folha->folhaPagamentoHasDesconto()->get();
        $pdf = Pdf::loadView('pdf.folhadepagamento', compact('funcionario', 'folha', 'adicionais', 'descontos'));
        $nomePdf = $funcionario->nome . '_' . $folha->data;
        return $pdf->stream($nomePdf . '.pdf');
    }
}