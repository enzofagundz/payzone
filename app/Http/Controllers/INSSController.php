<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreINSSRequest;
use App\Models\INSS;
use App\Services\INSSService;
use Illuminate\Contracts\View\View;

class INSSController extends Controller
{
    public function index(): View
    {
        return view("inss.index", [
            'inss' => INSS::orderBy('data_vigencia', 'desc')->paginate(10),
        ]);
    }

    public function create(): View
    {
        return view("inss.create");
    }

    public function store(StoreINSSRequest $request)
    {
        $data = $request->validated();        
        $data['percentual'] = (float) str_replace(',', '.', $data['percentual']);
        $data['valor_inicial'] = (float) str_replace(',', '.', $data['valor_inicial']);
        $data['valor_final'] = (float) str_replace(',', '.', $data['valor_final']);
        
        if (INSSService::verificarINSSCadastrado($data)) {
            return redirect()->back()->withErrors(['Já existe um INSS cadastrado com esses valores para a data de vigência.']);
        }
        
        INSS::create($data);
        return redirect()->route('inss.index');
    }

    public function show(string $id)
    {
        $inss = INSS::findOrFail($id);
        return view("inss.show", compact("inss"));
    }

    public function edit(string $id)
    {
        $inss = INSS::findOrFail($id);
        return view("inss.edit", compact("inss"));
    }

    public function update(StoreINSSRequest $request, string $id)
    {
        $data = $request->validated();
        $data['percentual'] = (float) str_replace(',', '.', $data['percentual']);
        $data['valor_inicial'] = (float) str_replace(',', '.', $data['valor_inicial']);
        $data['valor_final'] = (float) str_replace(',', '.', $data['valor_final']);
        
        $inss = INSS::findOrFail($id);
        $inss->update($data);
        return redirect()->route('inss.index');
    }

    public function destroy(string $id)
    {
        $inss = INSS::findOrFail($id);
        $inss->delete();
        return redirect()->route('inss.index');
    }
}
