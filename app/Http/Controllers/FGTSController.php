<?php

namespace App\Http\Controllers;

use App\Models\FGTS;
use App\Services\FGTSService;
use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use App\Http\Requests\StoreFGTSRequest;

class FGTSController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(): View
    {
        return view('fgts.index', [
            'fgts' => DB::table('fgts')->orderBy('data_vigencia', 'desc')->paginate(10),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): View
    {
        return view('fgts.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreFGTSRequest $request): RedirectResponse
    {
        $data = $request->validated();
        $data['percentual'] = (float) str_replace(',', '.', $data['percentual']);

        if(FGTSService::verificarFGTSCadastrado($data)) {
            return redirect()->back()->withErrors(['Já existe um FGTS cadastrado com esse percentual para a data de vigência.']);
        }

        FGTS::create($data);
        return redirect()->route('fgts.index');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id): View
    {
        $fgts = FGTS::findOrFail($id);
        return view('fgts.show', compact('fgts'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id): View
    {
        $fgts = FGTS::findOrFail($id);
        return view('fgts.edit', compact('fgts'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(StoreFGTSRequest $request, string $id): RedirectResponse
    {
        $data = $request->validated();

        $data['percentual'] = (float) str_replace(',', '.', $data['percentual']);

        $fgts = FGTS::findOrFail($id);
        $fgts->update($data);
        return redirect()->route('fgts.index');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id): RedirectResponse
    {
        $fgts = FGTS::findOrFail($id);
        $fgts->delete();
        return redirect()->route('fgts.index');
    }
}
