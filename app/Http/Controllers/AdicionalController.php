<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreAdicionalRequest;
use App\Models\Adicional;
use App\Models\Funcionario;
use App\Services\AdicionalService;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class AdicionalController extends Controller
{
    public function index(Funcionario $funcionario): View
    {
        return view('adicionais.index', [
            'adicionais' => $funcionario->funcionarioHasAdicional()->paginate(10),
            'funcionario' => $funcionario,
        ]);
    }

    public function create(Funcionario $funcionario): View
    {
        return view('adicionais.create', compact('funcionario'));
    }

    public function store(StoreAdicionalRequest $request, Funcionario $funcionario): RedirectResponse
    {
        // faz a validação do request em app/Http/Requests/StoreAdicionalRequest.php
        $data = $request->validated();

        // converte o percentual para float
        $data['percentual'] = (float) str_replace(',', '.', $data['percentual'] ?? 0);
        $data['salario_base'] = (float) $funcionario->salario()->latest('created_at')->first()->valor;
        
        // adiciona o funcionário ao array
        $data['funcionario'] = $funcionario;

        // adiciona o id do funcionário ao array
        $data['funcionario_id'] = $funcionario->id;

        // calcula o valor do adicional
        $adicional = AdicionalService::criarAdicional($data['nome']);
        $data['valor'] = $adicional->calcularAdicional($data);

        // cria o adicional
        Adicional::create($data);

        // redireciona para a página index de adicionais
        return redirect()->route('adicional.index', $funcionario);
    }

    public function show(Funcionario $funcionario, Adicional $adicional): View
    {
        return view('adicionais.show', compact('adicional',  'funcionario'));
    }

    public function destroy(Funcionario $funcionario, Adicional $adicional): RedirectResponse
    {
        $adicional->delete();
        return redirect()->route('adicional.index', $funcionario);
    }
}