<?php

namespace App\Http\Controllers;

use App\Services\HomeService;
use Illuminate\Contracts\View\View;

class HomeController extends Controller
{   

    public function index(): View
    {
        $pages = HomeService::getPages();
        return view('home', compact('pages'));
    }
}
