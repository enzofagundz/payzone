<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreFuncionarioRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'name' => 'required|string|max:255',
            'data_admissao' => 'required|date',
            'cpf' => ['required', 'cpf'],
            'carga_horaria' => 'required|numeric',
            'cargo_id' => 'required|numeric',
            'salario' => 'required|string',
        ];
    }
}
