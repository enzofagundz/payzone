<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreINSSRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'percentual' => 'required|string|between:0,100',
            'valor_inicial' => 'required|string|between:0,1000000',
            'valor_final' => 'nullable|string|between:0,1000000',
            'data_vigencia' => 'required|date',
        ];
    }
}
