<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreDescontoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'nome' => 'required|string|max:255',
            'percentual' => 'nullable|string|max:30|between:0,100',
            'data_vigencia' => 'required|date',
            'faltas' => 'nullable|numeric',
            'quantidadeValesUtilizados' => 'nullable|numeric',
            'valorPorVale' => 'nullable|string',
            'valor' => 'nullable'
        ];
    }
}
