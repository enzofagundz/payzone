<?php

namespace App\Models;

use App\Models\Funcionario;
use Illuminate\Database\Eloquent\Model;
use App\Models\FolhaPagamentoHasDesconto;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Desconto extends Model
{
    use HasFactory;

    protected $fillable = [
        'nome',
        'valor',
        'data_vigencia',
        'percentual',
        'funcionario_id',
    ];

    public function folhaPagamentoHasDesconto(): HasMany
    {
        return $this->hasMany(FolhaPagamentoHasDesconto::class);
    }

    // desconto belongs to funcionario

    public function funcionario(): BelongsTo
    {
        return $this->belongsTo(Funcionario::class);
    }
}
