<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\FuncionarioHasDependente;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Dependente extends Model
{
    use HasFactory;

    protected $fillable = [
        'nome',
        'cpf',
        'data_nascimento',
        'funcionario_id',
    ];

    public function funcionario(): HasMany {
        return $this->hasMany(FuncionarioHasDependente::class);
    }
}
