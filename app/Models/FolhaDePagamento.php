<?php

namespace App\Models;

use App\Models\INSS;
use App\Models\IRRF;
use App\Models\TipoFolha;
use App\Models\Funcionario;
use Illuminate\Database\Eloquent\Model;
use App\Models\FolhaPagamentoHasDesconto;
use App\Models\FolhaPagamentoHasAdicional;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class FolhaDePagamento extends Model
{
    use HasFactory;

    protected $table = 'folhas_de_pagamento';

    protected $fillable = [
        'tipo_folha_id',
        'funcionario_id',
        'irrf_id',
        'inss_id',
        'data',
    ];

    public function folhaPagamentoHasAdicional(): HasMany
    {
        return $this->hasMany(FolhaPagamentoHasAdicional::class);
    }

    public function folhaPagamentoHasDesconto(): HasMany 
    {
        return $this->hasMany(FolhaPagamentoHasDesconto::class);
    }

    public function tipoFolha(): BelongsTo
    {
        return $this->belongsTo(TipoFolha::class);
    }

    public function funcionario(): BelongsTo
    {
        return $this->belongsTo(Funcionario::class);
    }

    public function irrf(): BelongsTo
    {
        return $this->belongsTo(IRRF::class);
    }

    public function inss(): BelongsTo
    {
        return $this->belongsTo(INSS::class);
    }

    public function folhaCalculada(): HasOne
    {
        return $this->hasOne(FolhaCalculada::class);
    }
}
