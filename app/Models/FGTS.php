<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FGTS extends Model
{
    use HasFactory;

    protected $table = 'fgts';

    protected $fillable = [
        'percentual',
        'data_vigencia'
    ];
}
