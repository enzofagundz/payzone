<?php

namespace App\Models;

use App\Models\Adicional;
use App\Models\FolhaDePagamento;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class FolhaPagamentoHasAdicional extends Model
{
    use HasFactory;

    protected $table = 'folha_pagamento_has_adicionais';
    protected $fillable = [
        'folha_de_pagamento_id',
        'adicional_id',
    ];

    public function adicional(): BelongsTo
    {
        return $this->belongsTo(Adicional::class);
    }

    public function folhaPagamento(): BelongsTo
    {
        return $this->belongsTo(FolhaDePagamento::class);
    }
}
