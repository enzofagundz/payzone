<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Relatorio extends Model
{
    use HasFactory;

    protected $fillable = [
        'salario_bruto',
        'adicionais',
        'descontos',
        'inss',
        'fgts',
        'irrf',
        'data',
        'tipo',
        'custo',
    ];
}
