<?php

namespace App\Models;

use App\Models\FolhaDePagamento;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class FolhaCalculada extends Model
{
    use HasFactory;

    protected $table = 'folha_calculadas';

    protected $fillable = [
        'salario_adicionais',
        'salario_descontos',
        'fgts',
        'inss_calculado',
        'irrf_calculado',
        'salario_liquido',
        'folha_de_pagamento_id',
        'percentual_inss',
        'percentual_irrf',
        'base_irrf',
        'quantidade_dependentes',
        'salario_bruto',
    ];

    public function folhaDePagamento(): BelongsTo
    {
        return $this->belongsTo(FolhaDePagamento::class);
    }
}
