<?php

namespace App\Models;

use App\Models\Dependente;
use App\Models\Funcionario;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class FuncionarioHasDependente extends Model
{
    use HasFactory;

    protected $fillable = [
        'relacao',
        'funcionario_id',
        'dependente_id',
    ];

    public function dependente(): BelongsTo
    {
        return $this->belongsTo(Dependente::class);
    }

    public function funcionario(): BelongsTo
    {
        return $this->belongsTo(Funcionario::class);
    }
}
