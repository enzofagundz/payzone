<?php

namespace App\Models;

use App\Models\Funcionario;
use Illuminate\Database\Eloquent\Model;
use App\Models\FolhaPagamentoHasAdicional;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Adicional extends Model
{
    use HasFactory;

    protected $table = 'adicionais';

    protected $fillable = [
        'nome',
        'valor',
        'data_vigencia',
        'percentual',
        'funcionario_id',
    ];

    public function folhaPagamentoHasAdicional(): HasMany
    {
        return $this->hasMany(FolhaPagamentoHasAdicional::class);
    }

    public function funcionario(): BelongsTo
    {
        return $this->belongsTo(Funcionario::class);
    }
}
