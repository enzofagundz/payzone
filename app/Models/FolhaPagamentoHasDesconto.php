<?php

namespace App\Models;

use App\Models\Desconto;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class FolhaPagamentoHasDesconto extends Model
{
    use HasFactory;

    protected $fillable = [
        'folha__de_pagamento_id',
        'desconto_id',
    ];

    public function desconto(): BelongsTo
    {
        return $this->belongsTo(Desconto::class);
    }
}
