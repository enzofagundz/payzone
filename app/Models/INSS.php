<?php

namespace App\Models;

use App\Models\FolhaDePagamento;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class INSS extends Model
{
    use HasFactory;

    protected $table = 'inss';

    protected $fillable = [
        'percentual',
        'data_vigencia',
        'valor_inicial',
        'valor_final',
    ];

    public function folhaPagamento(): HasMany
    {
        return $this->hasMany(FolhaDePagamento::class);
    }
}
