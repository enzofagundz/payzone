<?php

namespace App\Models;

use App\Models\Cargo;
use App\Models\Salario;
use App\Models\Desconto;
use App\Models\Adicional;
use App\Models\FuncionarioHasDependente;
use Illuminate\Database\Eloquent\Model; 
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Funcionario extends Model
{
    use HasFactory;

    protected $fillable = [
        'nome',
        'cpf',
        'carga_horaria',
        'data_admissao',
        'data_demissao',
        'cargo_id'
    ];

    public function cargo(): BelongsTo
    {
        return $this->belongsTo(Cargo::class);
    }

    public function funcionarioHasDependente(): HasMany
    {
        return $this->hasMany(FuncionarioHasDependente::class);
    }

    public function salario(): HasMany
    {
        return $this->hasMany(Salario::class);
    }

    public function folhaDePagamento(): HasMany
    {
        return $this->hasMany(FolhaDePagamento::class);
    }

    // one to many funcionario has desconto

    public function funcionarioHasDesconto(): HasMany
    {
        return $this->hasMany(Desconto::class);
    }

    // one to many funcionario has adicional

    public function funcionarioHasAdicional(): HasMany
    {
        return $this->hasMany(Adicional::class);
    }
}