<?php

namespace App\Models;

use App\Models\FolhaDePagamento;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class TipoFolha extends Model
{
    use HasFactory;

    protected $fillable = [
        'nome',
    ];

    public function folhaPagamento(): HasMany
    {
        return $this->hasMany(FolhaDePagamento::class);
    }
}
