<?php

namespace App\Operacoes\Adicionais;

use App\Contracts\AdicionalCalculavel;

class AdicionalHorasExtras implements AdicionalCalculavel
{
    public function calcularAdicional($data): float
    {
        $valorHoraExtra = ($data['salario_base'] / $data['funcionario']->carga_horaria) * 1.5;
        return $data['horas_trabalhadas'] * $valorHoraExtra;
    }
}