<?php

namespace App\Operacoes\Adicionais;

use App\Contracts\AdicionalCalculavel;

class AdicionalNoturno implements AdicionalCalculavel
{
    public function calcularAdicional($data): float
    {
        return ($data['salario_base'] / 220) * 70 * (1 + ($data['percentual'] / 100));
    }
}