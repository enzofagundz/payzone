<?php

namespace App\Operacoes\Adicionais;

use App\Contracts\AdicionalCalculavel;

class AdicionalPericulosidade implements AdicionalCalculavel
{
    public function calcularAdicional($data): float
    {
        return ($data['salario_base'] / 100) * ($data['percentual'] ?? 30 / 100);
    }
}