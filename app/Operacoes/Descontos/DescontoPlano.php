<?php

namespace App\Operacoes\Descontos;

use App\Contracts\DescontoCalculavel;

class DescontoPlano implements DescontoCalculavel
{
    public function calcularDesconto($data): float
    {
        return $data['salario_base'] * ($data['percentual'] / 100);
    }
}
