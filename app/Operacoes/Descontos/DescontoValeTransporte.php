<?php

namespace App\Operacoes\Descontos;

use App\Contracts\DescontoCalculavel;

class DescontoValeTransporte implements DescontoCalculavel
{
    public function calcularDesconto($data): float
    {
        // Calcula o total a ser retido pelo salário do funcionário
        $valorRetencao = $data['salario_base'] * ($data['percentual'] / 100);
        // Calcula o total a ser pago pelos vales
        $precoTotalVales = $data['quantidadeValesUtilizados'] * $data['valorPorVale'];
        // Se o valor exceder ao valor retido pelo salário do funcionário a empresa custia
        if ($valorRetencao < $precoTotalVales){
            $excesso = $precoTotalVales - $data['valor']; //valor excedente a ser pago pela empresa
            return $data['valor'] = $valorRetencao;
        } // se não exceder
        else{
            $excesso = $precoTotalVales - $data['valor'];
            return $data['valor'] = $precoTotalVales;
        }
    }
}
