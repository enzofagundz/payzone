<?php

namespace App\Operacoes\Descontos;

use App\Contracts\DescontoCalculavel;

class DescontoValeAlimentacao implements DescontoCalculavel
{
    public function calcularDesconto($data): float
    {
        return $data['valor'] * ($data['percentual']/100);
    }
}
