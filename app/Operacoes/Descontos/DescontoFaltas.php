<?php

namespace App\Operacoes\Descontos;

use App\Contracts\DescontoCalculavel;

class DescontoFaltas implements DescontoCalculavel
{
    public function calcularDesconto($data): float
    {
        $desconto = $data['salario_base'] / 30;
        return $data['faltas'] * $desconto;
    }
}
