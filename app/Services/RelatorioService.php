<?php

namespace App\Services;

use Illuminate\Support\Facades\DB;

class RelatorioService
{

    public static function calcularRelatorioAnual($relatorio)
    {
        // vou receber um array de folhas de pagamento
        // preciso separar os meses e calcular o total de cada mês
        // depois calcular o total anual
        $relatorioAnual = [];
        $meses = [];
        foreach ($relatorio as $folha) {
            $mes = date('m', strtotime($folha->data));
            $meses[$mes][] = $folha;
        }

        // cada mês do relatório deve ter um total de salário bruto, salário líquido, descontos e adicionais
        foreach ($meses as $mes => $folhas) {
            $totalSalarioBruto = 0;
            $totalSalarioLiquido = 0;
            $totalDescontos = 0;
            $totalAdicionais = 0;
            foreach ($folhas as $folha) {
                $totalSalarioBruto += $folha->folhaCalculada->salario_bruto;
                $totalSalarioLiquido += $folha->folhaCalculada->salario_liquido;
                $totalDescontos += $folha->folhaCalculada->salario_descontos;
                $totalAdicionais += $folha->folhaCalculada->salario_adicionais;
            }
            $relatorioAnual[$mes] = [
                'totalSalarioBruto' => $totalSalarioBruto,
                'totalSalarioLiquido' => $totalSalarioLiquido,
                'totalDescontos' => $totalDescontos,
                'totalAdicionais' => $totalAdicionais,
                'mes' => $mes,
            ];
        }

        // calcular o total anual
        return $relatorioAnual;
    }
}
