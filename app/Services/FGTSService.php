<?php

namespace App\Services;

use App\Models\FGTS;
use Illuminate\Support\Facades\DB;

class FGTSService
{
    public static function calcularFGTS(Float $salarioBase, Float $aliquotaFGTS)
    {
        return $salarioBase * $aliquotaFGTS;
    }

    public static function filtrarFGTS(String $dataFolha)
    {
        $ano = date('Y', strtotime($dataFolha));
        $fgts = DB::table('fgts')
            ->whereYear('data_vigencia', $ano)
            ->first();
        return $fgts;
    }

    public static function verificarFGTSCadastrado($data)
    {
        $fgts = FGTS::where('data_vigencia', $data['data_vigencia'])->get();

        if (!$fgts) {
            return false;
        }

        foreach ($fgts as $fgts) {
            if ($data['percentual'] == $fgts->percentual) {
                return true;
            }
        }
    }
}
