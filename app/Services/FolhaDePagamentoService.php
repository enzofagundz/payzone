<?php

namespace App\Services;

use Exception;
use App\Models\Funcionario;
use App\Services\FGTSService;
use App\Services\INSSService;
use App\Services\IRRFService;
use App\Services\DescontoService;
use App\Services\AdicionalService;
use App\Services\DependenteService;

class FolhaDePagamentoService {
    public static function calcularFolhaPagamento(Funcionario $funcionario, String $data)
    {
        // pega o salário bruto do funcionário
        $salarioBruto = (float) $funcionario->salario()->latest('created_at')->first()->valor;
        
        // calcula o INSS
        $percentualINSS = INSSService::calcularPercentualINSS($salarioBruto, $data);
        $percentualINSS ? $inss = INSSService::calcularINSS($salarioBruto, $percentualINSS->percentual/100) : $inss = INSSService::calcularINSS($salarioBruto, 0);
        
        // calcula os dependentes
        $quantidadeDependentes = $funcionario->funcionarioHasDependente()->count();
        $descontoDependentes = DependenteService::calcularDescontoDependentes($quantidadeDependentes);

        // salario com desconto do INSS e dependentes
        $baseIRRF = $salarioBruto - $inss - $descontoDependentes;

        // calcula o FGTS
        $aliquotaFGTS = FGTSService::filtrarFGTS($data);
        
        if(!$aliquotaFGTS) {
            throw new Exception('Não foi possível encontrar a alíquota do FGTS');
        }
        
        $fgts = FGTSService::calcularFGTS($salarioBruto, $aliquotaFGTS->percentual/100);

        // calcula o IRRF com base no salário gerado com o desconto do INSS
        $aliquotaIRRF = IRRFService::calcularAliquotaIRRF($baseIRRF, $data);
        $aliquotaIRRF ? $irrf = IRRFService::calcularIRRF($baseIRRF, $aliquotaIRRF->percentual/100) : $irrf = IRRFService::calcularIRRF($baseIRRF, 0);

        $ano = date('Y', strtotime($data));
        $mes = date('m', strtotime($data));

        $adicionais = $funcionario->funcionarioHasAdicional()
            ->whereYear('data_vigencia', $ano)
            ->whereMonth('data_vigencia', $mes)
            ->get(); 
            
        $descontos = $funcionario->funcionarioHasDesconto()
            ->whereYear('data_vigencia', $ano)
            ->whereMonth('data_vigencia', $mes)
            ->get();


        // calcula os adicionais
        // pode ser que o funcionário não tenha adicionais $adicionais seja um array vazio
        if($adicionais->count() > 0) {
            $salarioComAdicionais = AdicionalService::calcularAdicionais($salarioBruto - $inss, $adicionais);
        } else {
            $salarioComAdicionais = $salarioBruto;
        }

        // calcula os descontos
        if($descontos->count() > 0) {
            $salarioComDescontos = DescontoService::calcularDescontos($salarioComAdicionais, $descontos);
        } else {
            $salarioComDescontos = $salarioComAdicionais;
        }

        return [
            'salario_adicionais' => $salarioComAdicionais,
            'salario_descontos' => $salarioComDescontos,
            'fgts' => $fgts,
            'inss_calculado' => $inss,
            'irrf_calculado' => $irrf,
            'salario_liquido' => $salarioComDescontos,
            'inss' => $percentualINSS,
            'irrf' => $aliquotaIRRF,
            'adicionais' => $adicionais,
            'descontos' => $descontos,
            'percentual_inss' => $percentualINSS->percentual,
            'percentual_irrf' => $aliquotaIRRF->percentual,
            'base_irrf'=> $baseIRRF,
            'quantidade_dependentes' => $quantidadeDependentes,
            'salario_bruto' => $salarioBruto,
        ];
    }
}