<?php

namespace App\Services;

use App\Models\INSS;
use PhpParser\Node\Expr\Cast\Double;

class INSSService {
    public static function calcularPercentualINSS(Float $salarioBase, String $dataFolha)
    {
        $ano = date('Y', strtotime($dataFolha));

        $inss = INSS::whereYear('data_vigencia', $ano)
            ->where('valor_inicial', '<=', $salarioBase)
            ->where('valor_final', '>=', $salarioBase)
            ->first();
        
        if (!$inss) {
            return false;
        }

        return $inss;
    }
    
    public static function calcularINSS(Float $salarioBruto, Float $percentualINSS) 
    {
        return $salarioBruto * $percentualINSS;
    }

    public static function verificarINSSCadastrado($data)
    {
        $inss = INSS::where('data_vigencia', $data['data_vigencia'])->get();

        if (!$inss) {
            return false;
        }

        foreach ($inss as $inss) {
            if ($data['valor_inicial'] == $inss->valor_inicial || $data['valor_final'] == $inss->valor_final) {
                return true;
            }
        }
    }
}