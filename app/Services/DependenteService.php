<?php

namespace App\Services;

class DependenteService {
    public static function calcularDescontoDependentes($numeroDependentes) {
        // não precisa tirar o salário base
        // ter um lugar para armazenar o valor do desconto
        return $numeroDependentes * 189.59;
    }

    public static function verificarIdadeDependente($data) {
        $idade = date_diff(date_create($data['data_nascimento']), date_create('now'))->y;
        return $idade > 21 && $data['relacao'] == 'filho' ? true : false;
    }
}