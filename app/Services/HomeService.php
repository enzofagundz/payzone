<?php

namespace App\Services;

class HomeService {
    public static function getPages() {
        $pages = [
            [
                'title' => 'Funcionários',
                'description' => 'Gerencie os funcionários da sua empresa',
                'link' => route('funcionarios.index'),
            ],
            [
                'title' => 'Cargos',
                'description' => 'Gerencie os cargos da sua empresa',
                'link' => route('cargos.index'),
            ],
            [
                'title' => 'INSS',
                'description' => 'Gerencie o INSS da sua empresa',
                'link' => route('inss.index'),
            ],
            [
                'title' => 'IRRF',
                'description' => 'Gerencie o IRRF da sua empresa',
                'link' => route('irrf.index'),
            ],
            [
                'title' => 'FGTS',
                'description' => 'Gerencie o FGTS da sua empresa',
                'link' => route('fgts.index'),
            ],
            [
                'title' => 'Relatórios',
                'description' => 'Gerencie os relatórios da sua empresa',
                'link' => route('relatorios.index'),
            ]
        ];

        return $pages; 
    }
}