<?php

namespace App\Services;

use App\Contracts\DescontoCalculavel;

class DescontoService
{
    public static function calcularDescontos(Float $salarioBase, Object $descontos)
    {
        $totalDescontos = 0;
        foreach ($descontos as $desconto) {
            $totalDescontos += (float) $desconto->valor;
        }
        return $salarioBase - $totalDescontos;
    }

    public static function criarDesconto(string $tipoDesconto): DescontoCalculavel
    {
        $classe = 'App\\Operacoes\\Descontos\\Desconto' . $tipoDesconto;
        
        if (!class_exists($classe)) {
            throw new \Exception("Classe de desconto não existe");
        }

        return new $classe;
    }
}
