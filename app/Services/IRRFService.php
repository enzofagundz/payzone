<?php

namespace App\Services;

use App\Models\IRRF;

class IRRFService {
    public static function calcularAliquotaIRRF(Float $salarioBruto, String $dataFolha)
    {
        $ano = date('Y', strtotime($dataFolha));

        $aliquotaIRRF = IRRF::whereYear('data_vigencia', $ano)
            ->where('valor_inicial', '<=', $salarioBruto)
            ->where('valor_final', '>=', $salarioBruto)
            ->first();

        if (!$aliquotaIRRF) {
            return false;
        }

        return $aliquotaIRRF;
    }

    public static function calcularIRRF($salarioBruto, $aliquotaIRRF) {
        return $salarioBruto * $aliquotaIRRF;
    }

    public static function baseIRRF($salarioBruto, $inss) {
        return $salarioBruto - $inss;
    }

    public static function verificarIRRFCadastrado($data) {
        $irrf = IRRF::where('data_vigencia', $data['data_vigencia'])->get();

        if (!$irrf) {
            return false;
        }

        foreach ($irrf as $irrf) {
            if ($data['valor_inicial'] == $irrf->valor_inicial || $data['valor_final'] == $irrf->valor_final) {
                return true;
            }
        }
    }
}