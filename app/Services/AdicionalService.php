<?php

namespace App\Services;

use App\Contracts\AdicionalCalculavel;

class AdicionalService
{
    public static function calcularAdicionais(Float $salarioBruto, Object $adicionais): Float
    {
        $totalAdicionais = 0.0;
        foreach ($adicionais as $adicional) {
            $totalAdicionais += (float) $adicional->valor;
        }

        return $salarioBruto + $totalAdicionais;
    }

    public static function criarAdicional(string $tipoAdicional): AdicionalCalculavel
    {
        $classe = 'App\\Operacoes\\Adicionais\\Adicional' . $tipoAdicional;
        
        if (!class_exists($classe)) {
            throw new \Exception("Classe de adicional não existe");
        }

        return new $classe;
    }
}