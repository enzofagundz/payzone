<?php

namespace App\Contracts;

interface AdicionalCalculavel
{
    public function calcularAdicional($data);
}