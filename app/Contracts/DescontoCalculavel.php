<?php

namespace App\Contracts;

interface DescontoCalculavel
{
    public function calcularDesconto($data);
}