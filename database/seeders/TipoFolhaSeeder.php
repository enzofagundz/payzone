<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class TipoFolhaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('tipo_folhas')->insert([
            'nome' => 'Folha de Pagamento',
        ]);
        DB::table('tipo_folhas')->insert([
            'nome' => 'Férias',
        ]);
        DB::table('tipo_folhas')->insert([
            'nome' => '13º Salário',
        ]);
        DB::table('tipo_folhas')->insert([
            'nome' => 'Rescisão',
        ]);
    }
}