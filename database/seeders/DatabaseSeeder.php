<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $this->call([
            TipoFolhaSeeder::class,
            CargosSeeder::class,
            INSSSeeder::class,
            IRRFSeeder::class,
            FGTSSeeder::class,
        ]);
    }
}
