<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class INSSSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('inss')->insert([
            'valor_inicial' => 0,
            'valor_final' => 1302,
            'percentual' => 7.5,
            'data_vigencia' => '2023-01-01'
        ]);
        DB::table('inss')->insert([
            'valor_inicial' => 1302.01,
            'valor_final' => 2571.29,
            'percentual' => 9,
            'data_vigencia' => '2023-01-01'
        ]);
        DB::table('inss')->insert([
            'valor_inicial' => 2571.30,
            'valor_final' =>3856.94,
            'percentual' => 12,
            'data_vigencia' => '2023-01-01'
        ]);
        DB::table('inss')->insert([
            'valor_inicial' => 3856.95,
            'valor_final' => 7507.49,
            'percentual' => 14,
            'data_vigencia' => '2023-01-01'
        ]);
    }
}
