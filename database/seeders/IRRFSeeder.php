<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class IRRFSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('irrf')->insert([
            'valor_inicial' => 0,
            'valor_final' => 1903.98,
            'percentual' => 0,
            'data_vigencia' => '2023-01-01'
        ]);
        DB::table('irrf')->insert([
            'valor_inicial' => 1903.99,
            'valor_final' =>2826.65,
            'percentual' => 7.5,
            'data_vigencia' => '2023-01-01'
        ]);
        DB::table('irrf')->insert([
            'valor_inicial' => 2826.66,
            'valor_final' =>3751.05,
            'percentual' => 15,
            'data_vigencia' => '2023-01-01'
        ]);
        DB::table('irrf')->insert([
            'valor_inicial' => 3751.06,
            'valor_final' => 4664.68,
            'percentual' => 14,
            'data_vigencia' => '2023-01-01'
        ]);
    }
}
