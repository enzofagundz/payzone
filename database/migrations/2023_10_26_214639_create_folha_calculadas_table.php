<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('folha_calculadas', function (Blueprint $table) {
            $table->id();
            $table->decimal('salario_adicionais', 10, 2)->nullable();
            $table->decimal('salario_descontos', 10, 2)->nullable();
            $table->decimal('fgts', 10, 2)->nullable();
            $table->decimal('inss_calculado', 10, 2)->nullable();
            $table->decimal('irrf_calculado', 10, 2)->nullable();
            $table->decimal('salario_liquido', 10, 2)->nullable();
            $table->decimal('percentual_inss', 10, 2)->nullable();
            $table->decimal('percentual_irrf', 10, 2)->nullable();
            $table->decimal('base_irrf', 10, 2)->nullable();
            $table->integer('quantidade_dependentes')->nullable();
            $table->decimal('salario_bruto', 10, 2)->nullable();
            $table->unsignedBigInteger('folha_de_pagamento_id');
            $table->foreign('folha_de_pagamento_id')->references('id')->on('folhas_de_pagamento')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('folha_calculadas');
    }
};
