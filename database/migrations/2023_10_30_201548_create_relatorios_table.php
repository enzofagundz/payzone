<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('relatorios', function (Blueprint $table) {
            $table->id();
            $table->decimal('salario_bruto', 10, 2);
            $table->decimal('adicionais', 10, 2);
            $table->decimal('descontos', 10, 2);
            $table->decimal('inss', 10, 2);
            $table->decimal('fgts', 10, 2);
            $table->decimal('irrf', 10, 2);
            $table->date('data');
            $table->integer('tipo');
            $table->decimal('custo', 10, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('relatorios');
    }
};
