<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('folha_pagamento_has_descontos', function (Blueprint $table) {
            $table->id();
            $table->foreignId('folha_de_pagamento_id')->constrained('folhas_de_pagamento')->onDelete('cascade')->onUpdate('cascade');
            $table->foreignId('desconto_id')->constrained('descontos')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('folha_pagamento_has_descontos');
    }
};
