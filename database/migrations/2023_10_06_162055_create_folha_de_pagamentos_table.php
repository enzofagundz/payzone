<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('folhas_de_pagamento', function (Blueprint $table) {
            $table->id();
            $table->date('data');
            $table->foreignId('tipo_folha_id')->constrained('tipo_folhas');
            $table->foreignId('funcionario_id')->constrained('funcionarios')->onDelete('cascade');
            $table->foreignId('inss_id')->constrained('inss');
            $table->foreignId('irrf_id')->constrained('irrf');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('folhas_de_pagamento');
    }
};
