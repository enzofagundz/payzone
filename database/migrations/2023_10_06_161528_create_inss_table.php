<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('inss', function (Blueprint $table) {
            $table->id();
            $table->decimal('percentual', 10, 2);
            $table->decimal('valor_inicial', 10, 2);
            $table->decimal('valor_final', 10, 2)->nullable();
            $table->date('data_vigencia');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('inss');
    }
};
